﻿using System.ComponentModel.DataAnnotations;

namespace CardFileOfTextMaterials.Models
{
    public class LoginModel
    {
       [Required(ErrorMessage = "User Name is required")]
       public string UserName { get; set; }
       [Required(ErrorMessage = "Password is reuired")]
       public string Password { get; set; }
    }
}
