﻿using CardFileOfTextMaterials.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace CardFileOfTextMaterials.Contexts
{
    public class CardFileOfTextMaterialDbContext : IdentityDbContext<CardFileOfTextMaterialsUser>
    {
        public CardFileOfTextMaterialDbContext(DbContextOptions<CardFileOfTextMaterialDbContext>options): base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);  
        }
    }
}
