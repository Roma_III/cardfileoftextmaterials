﻿using System;
using CardFileOfTextMaterials.Enums;

namespace CardFileOfTextMaterials.Entities
{
    public class Book
    {
        public int Id { get; set; }
        public string BookName { get; set; }
        public AuthorBook AuthorBook { get; set; }
        public DateTime LastUpdate { get; set; }
        public Genres Genres { get; set; }
        public CardFileOfTextMaterialsUser User { get; set; }
        public string UserId { get; set; }
    }
}
