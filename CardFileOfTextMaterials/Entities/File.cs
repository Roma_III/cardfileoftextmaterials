﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CardFileOfTextMaterials.Entities
{
    public class File
    {
        public Guid FileId { get; set; }

        public string FileLocation { get; set; }

    }
}
